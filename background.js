function main(options) {
  function getRedirectURL(url) {
    try {
      const redirectUrl = new URL('https://www.youtube.com/' + options.redirectUrlPath);
      const params = new URLSearchParams(redirectUrl.search);
      params.set(options.redirectParamName, url);
      redirectUrl.search = params.toString();
      return redirectUrl.toString();
    } catch (e) {
      return url;
    }
  }

  function shouldRedirect(url) {
    try {
      const { pathname } = new URL(url);

      return options.redirectFix && (
        pathname === '/' ||
        pathname === '/index' ||
        pathname === '/watch' ||
        pathname === '/playlist' ||
        pathname.startsWith('/channel/') ||
        pathname.startsWith('/feed/') ||
        pathname.startsWith('/c/') ||
        pathname.startsWith('/user/')
      );
    } catch (e) {
      return false;
    }
  }

  chrome.webRequest.onBeforeRequest.addListener((details) => {
    const { url } = details;
    if (shouldRedirect(url)) {
      return {
        redirectUrl: getRedirectURL(url),
      };
    }
  }, {
    urls: [
      "https://www.youtube.com/*",
    ],
    types: ["main_frame"],
  }, ['blocking']);

  chrome.webRequest.onBeforeSendHeaders.addListener(({ requestHeaders }) => {
    const uaHeader = requestHeaders
      .find(header => header.name.toLowerCase() === "user-agent");

    uaHeader.value = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';

    return {
      requestHeaders,
    };
  }, {
    urls: ["*://www.youtube.com/*"],
    types: ["main_frame"],
  }, ['blocking', 'requestHeaders']);

  chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    switch (message.type) {
      case 'getOptions':
        sendResponse(options);
        break;
      case 'resetOption':
        resetOption(message.name);
        sendResponse(options);
        break;
    }

    if (message.type === 'getOptions') {
      sendResponse(options);
    }
  });
}

getOptions(main);
