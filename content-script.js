chrome.runtime.sendMessage({ type: 'getOptions' }, (options) => {
  // Restore "show more" button
  function restoreShowMoreButton() {
    if (!options.restoreShowMoreButton) {
      return;
    }

    const actionPanel = document.getElementById('action-panel-details');
    const showMore = document.getElementById('show-more-button_');

    if (!actionPanel || showMore) {
      return;
    }

    actionPanel.insertAdjacentHTML('beforeend',
      '<button class="yt-uix-button yt-uix-button-size-default yt-uix-button-expander yt-uix-expander-head yt-uix-expander-collapsed-body yt-uix-gen204" id="show-more-button_" type="button" onclick="return false;" data-gen204="feature=watch-show-more-metadata"><span class="yt-uix-button-content">Show more</span></button>' +
      '<button class="yt-uix-button yt-uix-button-size-default yt-uix-button-expander yt-uix-expander-head yt-uix-expander-body" type="button" onclick="return false;"><span class="yt-uix-button-content">Show less</span></button>'
    );
  }

  // Force page reload for new interface links
  document.documentElement.addEventListener('click', e => {
    if (options.forceNewUIHardLinks) {
      return;
    }

    let node = e.target;
    while (node && node.parentElement) {
      node.classList.remove('yt-simple-endpoint');
      node = node.parentElement;
    }
  }, true);

  if (!options.redirectFix) {
    return;
  }

  // Enable classic design on home page through redirect
  const redir = new URLSearchParams(location.search.replace('?', ''))
    .get(options.redirectParamName);

  if (redir) {
    document.documentElement.classList.add('oldtube_redirecting');
  }

  window.addEventListener('spfdone', () => {
    document.documentElement.classList.remove('oldtube_redirecting');
    restoreShowMoreButton();
  });

  window.addEventListener('DOMContentLoaded', () => {
    const searchInput = document.getElementById('masthead-search-term');
    if (searchInput && searchInput.value === '""') {
      searchInput.value = '';
    }

    restoreShowMoreButton();

    if (!document.querySelector('.spf-link')) {
      return;
    }

    if (redir) {
      const a = document.createElement('a');
      a.href = redir;
      a.className = 'spf-link';
      document.body.appendChild(a);
      a.click();
    }
  });
});
