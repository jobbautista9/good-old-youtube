const OPTIONS = ['redirectFix', 'redirectUrlPath', 'forceNewUIHardLinks', 'restoreShowMoreButton'];

function showCurrentOptions(options) {
  OPTIONS.forEach(id => {
    const input = document.getElementById(id);
    if (input.type === 'text') {
      input.value = options[id];
    } else {
      input[options[id] ? 'setAttribute' : 'removeAttribute']('checked', true);
    }
  });
}

getOptions((options) => {
  showCurrentOptions(options);

  ['redirectFix', 'redirectUrlPath', 'forceNewUIHardLinks', 'restoreShowMoreButton'].forEach(id => {
    const input = document.getElementById(id);
    input.addEventListener('change', () => setOption(id,
      input[input.type === 'text' ? 'value' : 'checked']
    ));
  });

  document.getElementById('resetRedirectURL').addEventListener('click', () => {
    resetOption('redirectUrlPath');
    showCurrentOptions(options);
  });
});
